#include <QString>
#include <QByteArray>
#include <QTextStream>
#include <QVector>
#include <QFile>
#include <QTextCodec>
#include <QLocale>
#include <QXmlStreamReader>
#include <QRegularExpression>
#include <QElapsedTimer>

QVector<QString> GetFileParams(QString path);

int main() {
  QElapsedTimer timer;
  timer.start();

  QString inFile; /*strFre, strRu, lang;*/
  QString outFile;

  //-------------------------------------------------------------------------------
  //============================== INIT ===========================================
  //-------------------------------------------------------------------------------
  QTextStream debugStream(stdout);
  debugStream.setCodec("UTF-8");

  //inFile = "D:/C++/build-xmlParser-Desktop_Qt_5_15_2_MinGW_64_bit-Debug/debug/neftegas_en-ru_1_.tmx";
  inFile = "D:/C++/build-xmlParser-Desktop_Qt_5_15_2_MinGW_64_bit-Debug/debug/la_redoute_fr-ru-231973.tmx";

  QVector<QString> parts = GetFileParams(inFile);

  QString path = parts[0];
  QString name = parts[1];

  QRegularExpression rename("(.*result_)([0-9]+)(.*)", QRegularExpression::UseUnicodePropertiesOption);
  QRegularExpressionMatch match = rename.match(name);

  if(match.hasMatch())
  {
    QString part2 = match.captured(2);

    if(part2.length() != 0)
    {
      int prt2 = part2.toInt();
      prt2++;
      part2 = QVariant(prt2).toString();
    }
    else
    {
      part2 = "1";
    }

    name.replace(rename, "\\1" + part2 + "\\3");
  }
  else
  {
    name = "result_" + name;
  }

  outFile = path + name;

  //QString lng("fr_ru");

  QFile in(inFile);
  if(!in.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    debugStream << "Can't read from file " << in.fileName() << "\n";
    return 1;
  }

  QFile out(outFile);
  if(!out.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    debugStream << "Can't read from file " << out.fileName() << "\n";
    return 2;
  }

  //QString ru_fre("ru_fr");
  //QString fre_ru("fr_ru");

  QXmlStreamReader reader(&in);
  QXmlStreamWriter writer(&out);

  writer.setAutoFormatting(true);
  writer.writeStartDocument();

  //-------------------------------------------------------------------------------
  //========================== PARSE TMX XML ======================================
  //-------------------------------------------------------------------------------
  // int count = 0;
  if(reader.readNextStartElement())
  {
    if (reader.name() == "tmx")
    {
      writer.writeCurrentToken(reader);
      while(!reader.atEnd() && !reader.hasError() && reader.readNext())
      {
        if(reader.name() == "header")
        {
          writer.writeStartElement("header");
          QXmlStreamAttributes attrs = reader.attributes();
          for(int i = 0; i < attrs.size(); i++)
          {
            writer.writeAttribute(attrs[i].name().toString(), "");
          }
          writer.writeEndElement();
          reader.skipCurrentElement();
        }
        else if(reader.name() == "body" && reader.tokenType() == QXmlStreamReader::StartElement)
        {
          writer.writeStartElement("body");
        }
        else if(reader.name() == "tu" && reader.tokenType() == QXmlStreamReader::StartElement)
        {
          writer.writeStartElement("tu");
        }
        else if(reader.name() == "tuv")
        {
          writer.writeCurrentToken(reader);
        }
        else if(reader.name() == "tu" && reader.tokenType() == QXmlStreamReader::EndElement)
        {
          writer.writeCurrentToken(reader);
        }
        else if(reader.name() == "seg")
        {
          writer.writeTextElement("seg", reader.readElementText(QXmlStreamReader::IncludeChildElements).trimmed());
        }
        else if(reader.name() == "body" && reader.tokenType() == QXmlStreamReader::EndElement)
        {
          writer.writeCurrentToken(reader);
        }
        else if(reader.name() == "tmx" && reader.tokenType() == QXmlStreamReader::EndElement)
        {
          writer.writeCurrentToken(reader);
          break;
        }
      }
    }
    else
    {
      reader.raiseError(QObject::tr("Incorrect file structure. File is not tmx."));
    }
  }
  else
  {
    reader.raiseError(QObject::tr("Incorrect file structure. File is not xml at all."));
  }
  if(reader.hasError())
  {
    debugStream << "Error while parsing xml occured:\n";
    debugStream << "\t" << "Error: " << reader.errorString() << " at line: "
      << reader.lineNumber() << " column: " << reader.columnNumber() << "\n";
    debugStream << reader.readElementText(QXmlStreamReader::IncludeChildElements) << "\n\n";
    out.close();
    out.remove();
    return 3;
  }
  debugStream << "Program took: " << ((timer.elapsed() / 1000) / 60 ) << " min, " << (timer.elapsed() / 100) % 60 << " sec, " << timer.elapsed() % 100 << " ms\n";
  return 0;
}

QVector<QString> GetFileParams(QString path)
{
  QRegularExpression getParams("(.*\\/)?(.*)", QRegularExpression::UseUnicodePropertiesOption);
  QRegularExpressionMatch match = getParams.match(path);
  QVector<QString> res;
  if(match.hasMatch())
  {
    res.push_back(match.captured(1));
    res.push_back(match.captured(2));
  }

  return res;
}
